import random 

print('영어 단어 번역 게임')
print('영어 단어를 번역하는 게임 입니다.')

dictionary = {
    'tiger': '호랑이', 
    'starfish': '불가사리',
    'shark': '상어',
    'mouse': '쥐',
    'banana': '바나나'
}

keys = list(dictionary.keys())
random.shuffle(keys)

count = 0
for english in keys:
    korean = dictionary[english]

    guess = input('{} 영어 단어를 번역하세요: '.format(english))

    if guess == korean: 
        print('영어 단어의 번역이 맞습니다.') 
        count += 1
    else: 
        print('영어 단어의 번역이 틀립니다.')

score = count / len(dictionary) * 100
print('100 점 만점 {} 점입니다.'.format(score))
#print('100 점 만점 {0} 점입니다.'.format(score))
